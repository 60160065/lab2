module.exports = {
    isUserNameValid: function(username){
        if(username.length < 3 || username.length > 15){
            return false;
        }
        //Kob -> kob
        if(username.toLowerCase() !== username){
            return false;
        }
        return true;
    },

    isAgeValid: function(age){
        const number = /^[0-9]+$/;
        if(!number.test(age)){
            return false;
        }
        if(age < 18 || age > 100){
            return false;
        }
        return true;
    },

    isPasswordValid: function(password){
        if(password.length < 8){
            return false;
        }
        const OneUpper = /(?=.*[A-Z])/
        if(!OneUpper.test(password)){
            return false;
        }
        const atLeastThreeDigits = /^(?:\D*\d){3}/
        if (!atLeastThreeDigits.test(password)) {
            return false;
        }
        const atLeastSpecial = /[\W_]/
        if (!atLeastSpecial.test(password)) {
            return false;
        }
        return true;
    },

    isDateValid: function(day,month,year){
        if (day < 1 || day > 31) {
            return false;
        }

        if (month < 1 || month > 12) {
            return false;
        }

        if (year < 1970 || year > 2020) {
            return false;
        }
        if(month == 1 || month == 3 || month == 5 || month == 7|| month == 8 || month == 10 || month == 12 ){
            if (day < 1 || day > 31) {
                return false;
            }
        }
        if(month == 2){
            if (year % 400 !== 0 && year % 100 !== 0 ) {
                if (day < 1 || day > 28) {
                    return false;
                }
            }
        }
        if(month == 4 || month == 6 || month == 9 || month == 11){
            if (day < 1 || day > 30) {
                    return false;
                }
            }
        return true;
    }
}
